import Twitter from './../components/Twitter'
import Navbar2 from './../components/Navbar2'
import Footer from './../components/Footer'
import {Container} from 'react-bootstrap'
import { SocialIcon } from 'react-social-icons';

export default function Social () {
	return (
		<>
		<Navbar2/>
		<Container fluid className="AboutContainer">
			<Container>
				<Twitter/>
			</Container>
			<h4 className="mt-3">social media links</h4>
			<Container className="mt-2">
				<SocialIcon className="sicon" url="https://www.facebook.com/ReeseLansangan" target="_blank"/>
				<SocialIcon url="https://www.instagram.com/reeseypeasy/?hl=en" target="_blank"/>
				<SocialIcon url="https://twitter.com/reeseypeasy?s=20" target="_blank"/>
				<SocialIcon url="https://www.youtube.com/user/reeselansangan" target="_blank"/>
				<SocialIcon url="https://open.spotify.com/artist/2JI8ViuZDBybY6Xd9ujUrb?si=7_Xh179NS_OXYV6uExnj1A&dl_branch=1" target="_blank" />
			</Container>
		</Container>
		<Footer/>
		</>
	)
}