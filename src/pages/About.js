import About2 from '../components/About'
import Navbar2 from './../components/Navbar2'
import Footer from './../components/Footer'

export default function About () {
	return (
		<>
		<Navbar2/>
		<About2/>
		<Footer/>
		</>
	)
}