import Albums from '../components/Albums'
import Navbar2 from './../components/Navbar2'
import Footer from './../components/Footer'

export default function Gallery () {
	return (
		<>
		<Navbar2/>
		<Albums/>
		<Footer/>
		</>
	)
} 