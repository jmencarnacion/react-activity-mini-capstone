import Banner from '../components/Banner'


//let's create the structure of our error page by creating a function called "error"

export default function Error () {
	const values={
		title:"404 - not found",
		content:"the page you are looking for cannot be found",
		label:"go back to home",
		destination:"/"
	}
	return (
		<>
			<Banner data={values}/>
		</>
	)
}