import './App.css';
import Home from './pages/Home'
import {BrowserRouter as Router} from 'react-router-dom'
import {Route, Switch} from 'react-router-dom'
// import {Container} from 'react-bootstrap'
import Error from './pages/Error'
import About from './pages/About'
import Gallery from './pages/Gallery'
import Social from './pages/Social'

export default function App() {
  return (
    <div className="App">
    
      <Router>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route exact path="/about" component={About} />
            <Route exact path="/gallery" component={Gallery} />
            <Route exact path="/social" component={Social} />
            <Route component={Error}/>
          </Switch>
      </Router>
    
    </div>
  )
}


// import {createElement} from 'react'
// import React from 'react'
//let's extract the subfunctions inside the module using a named export method
// import {Reminder, Message, Example} from './components/Greetings'
//2nd method of creating JSX as an object
// const structure =  (
//   <div>
//     <h1>Hello</h1>
//     <h1>Popoy</h1>
//     <p>Basya</p>
//   </div>  
// )
// //3rd method of crating an JSX using the createElement() constructor
// let component = createElement(
//   'h1', //element
//   {className:'welcome'}, //properties/attributes
//   'hello batch 99'//content
// )

//what would the previous method would look like if it is an HTML component

// let componentTranslation = <h1 className="welcome">hello batch 99</h1>
