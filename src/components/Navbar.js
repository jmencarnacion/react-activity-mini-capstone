import {Navbar, Nav, Jumbotron, Button, Container} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'

const NavBar = () => {
	return (
		<div>
			<Jumbotron fluid className="Jumbotron">
			  <Container fluid>
			    <Navbar expand="lg" className="NavBarMain" sticky="top" variant="dark">
			    	<Navbar.Brand as={Link} to="/">reese lansangan stan page</Navbar.Brand>
				    <Navbar.Toggle aria-controls="basic-navbar-nav" />
				  <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ml-auto">
				    	<Nav.Link as={NavLink} to="/about">about</Nav.Link>
						<Nav.Link as={NavLink} to="/gallery">gallery</Nav.Link>
				    	<Nav.Link as={NavLink} to="/social">social_media</Nav.Link>
				    </Nav>
				  </Navbar.Collapse>
				</Navbar>

{/*			    <Navbar expand="lg" className="Navbar" sticky="top" variant="dark">
				  <Navbar.Toggle aria-controls="basic-navbar-nav" />
				  <Navbar.Collapse id="basic-navbar-nav">
				    <Nav className="ml-auto">
				    	<Nav.Link href="https://www.reeselansangan.com/" target="_blank">official page</Nav.Link>
						<Nav.Link href="https://open.spotify.com/playlist/5WDoB9MpFgHy0J23hODvvO?si=44ecd356d3ae4074&nd=1" target="_blank">spotify</Nav.Link>
						<Nav.Link href="https://www.youtube.com/user/reeselansangan" target="_blank">youtube</Nav.Link>
						<Nav.Link href="https://www.facebook.com/ReeseLansangan" target="_blank">facebook</Nav.Link>
						<Nav.
						Link href="https://www.instagram.com/reeseypeasy/" target="_blank">instagram</Nav.Link>
						<Nav.Link href="https://twitter.com/reeseypeasy" target="_blank">twitter</Nav.Link>
				    </Nav>
				  </Navbar.Collapse>
				</Navbar>*/}
				
			  </Container>
			  <Container fluid className="welcome">
			  	<h1>#Reese2ndAlbum</h1>
			  	<p className="tinyP">out now!</p>
			  </Container>
			  <Container fluid className="button">
			  	<Button className="button1" variant="danger" href="https://open.spotify.com/playlist/5WDoB9MpFgHy0J23hODvvO?si=44ecd356d3ae4074" target="_blank">listen on spotify</Button>
			  	<Button className="button1" variant="danger" href="https://www.reeselansangan.com/" target="_blank">visit official site</Button>
			  </Container>
			</Jumbotron>
		</div>
	)
}

export default NavBar