//let's practice creating components using JSX syntax
//the purpose of this component is to be the hero section of our page
import {Jumbotron, Container, Button} from 'react-bootstrap' //i'll be using a name export to acquire my components from react bootstrap
//let's acquire the bootstrap grid system
import {Row, Col} from 'react-bootstrap'
import {Link} from 'react-router-dom'

// i will create a function that will return the structure of my component
//let's create a function in a es6 format
// let name = "jami encarnacion";

//our task is to pass down props inside our Banner component

const Banner = ({data}) => {
	//let's declare a return scope to determine the anatomy of the element.

	//let's destructure the data prop into its properties
	const {title, content, destination, label} = data

	return (
		<Container>
			<Row>
				<Col>
					<Jumbotron>
						<h1>{title}</h1>
						<p>{content}</p>
						<Button variant="success" className="mb-5" as={Link} to={destination}>{label}</Button>
					</Jumbotron>
				</Col>
			</Row>
		</Container>
	)
}

export default Banner