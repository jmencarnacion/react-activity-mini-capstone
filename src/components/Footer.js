import {Navbar, Nav} from 'react-bootstrap'

const Footer = () => {
	return (
		<>
			<Navbar expand="lg" className="Footer" sticky="top" variant="light">
				  <Navbar.Brand>disclaimer |</Navbar.Brand>
				    <Nav className="ml-auto">
				    	<Nav.Link href="#link">for educational purposes only. images and other materials are credited to the owners. this page is under Fair Use |
				    	</Nav.Link>
				    </Nav>
				</Navbar>
		</>
	)
}

export default Footer