import {Container, Row, Col, Carousel} from 'react-bootstrap'

const Albums = () => {
	return (
		<div fluid className="AlbumContainer1">
			<Container fluid className="AlbumContainer2">
				<Row>
					<Col md={6}>
						<h4>albums & EPs</h4>
						<Carousel variant="dark" nextLabel="" prevLabel="" >
						<Carousel.Item >
						<img className="carouselIMG" src={require('./../images/album1.jpg').default} height={500} width={500} alt="reeseprof"/>
						</Carousel.Item>
						<Carousel.Item >
						<img className="carouselIMG" src={require('./../images/album2.jpg').default} height={500} width={500} alt="reeseprof"/>
						</Carousel.Item>
						<Carousel.Item >
						<img className="carouselIMG" src={require('./../images/album3.jpg').default} height={500} width={500} alt="reeseprof"/>
						</Carousel.Item>
						<Carousel.Item >
						<img className="carouselIMG" src={require('./../images/album4.jpg').default} height={500} width={500} alt="reeseprof"/>
						</Carousel.Item>
						</Carousel>
					</Col>
					<Col md={6}>
						<h4>listen to her songs</h4>
							<iframe className="spotify" src="https://open.spotify.com/embed/playlist/5WDoB9MpFgHy0J23hODvvO" width="400" height="500" frameborder="0" allowtransparency="true" allow="encrypted-media" title="spotifyplaylist">
							</iframe>
					</Col>
				</Row>
			</Container>
		</div>
	)
}


export default Albums