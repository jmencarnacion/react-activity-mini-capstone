import {Timeline} from 'react-twitter-widgets'

export default function Twitter () {
	return (
		<>
 	 		<Timeline className="Timeline" dataSource={{ sourceType: "profile", screenName: "reeseypeasy" }} options={{ width: "1200", height: "600" }}/>
		</>
	)
}