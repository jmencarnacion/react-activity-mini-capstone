import {Navbar, Nav} from 'react-bootstrap'
import {Link, NavLink} from 'react-router-dom'

export default function Navbar2 () {
	return (
		<>
		<Navbar expand="lg" className="NavBarMain2" sticky="top" variant="dark">
			<Navbar.Brand as={Link} to="/">reese lansangan stan page</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} to="/about">about</Nav.Link>
					<Nav.Link as={NavLink} to="/gallery">gallery</Nav.Link>
					<Nav.Link as={NavLink} to="/social">social_media</Nav.Link>
				</Nav>
			</Navbar.Collapse>
		</Navbar>
		</>
	)
} 