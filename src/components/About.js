import {Container, Row, Col} from 'react-bootstrap'

const About = () => {
	return (
		<div>
			<Container fluid className="AboutContainer">
				<h2>about reese</h2>
				<Row>
					<Col lg={4}>
						<img src={require('./../images/reeseprof.jpg').default} height={300} width={300} alt="reeseprof"/>
					</Col>					
					<Col lg={4} className="description">
						<p>
							born Maria Therese Lansangan, is a Filipino musician, song writer, visual artist, graphic designer, fashion designer and published author. best known for her 2015 debut album 'Arigato Internet'
						</p>
						<p>
							she graduated from Ateneo De Manila University in 2011 with a degree in bachelor in fine arts in information design 
						</p>
						<p>
							please support her by streaming her songs, following her social media pages, this page is dedicated to support her dedication to her chosen craft
						</p>
					</Col>
					<Col className="imgContainer" lg={4}>
						<img src={require('./../images/reeseprof.jpg').default} height={300} width={300} alt="reeseprof"/>
					</Col>		
				</Row>
			</Container>
		</div>
	)
}

export default About